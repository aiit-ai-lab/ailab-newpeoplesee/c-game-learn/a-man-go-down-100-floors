# 是男人就下100层

这段代码实现了一个简单的控制台小游戏，游戏的目标是控制一个小球在屏幕上跳跃，躲避上升的障碍物（木板），并尽量获得高分。

**基本要素：**

- 玩家需要控制小球左右移动，躲避上升的木板，同时尽可能获得高分。

- 游戏随着得分的增加而增加难度，包括木板上升速度的增加和更多的木板出现。

- 游戏会在控制台窗口中实时显示，玩家通过键盘输入来控制小球的移动。

- 当小球触碰到上边界或木板时，游戏结束并显示得分。

## 代码简要介绍：

1. 导入必要的标准库头文件：stdio.h、stdlib.h、conio.h、windows.h、time.h。

2. 定义了一些常量：
   - `high` 和 `wide` 定义了游戏画面的高度和宽度。
   - `radius` 定义了小球的半径。
   - `interval_static` 定义了用于控制障碍物（木板）生成频率的时间间隔。
   - `interval_1` 和 `interval_2` 用于控制障碍物上升和小球下落的时间间隔。
   - `board_1`、`board_2` 和 `board_3` 用于记录三个木板的位置。
   - `score` 用于记录玩家的得分。
   - `turn_1` 和 `turn_2` 用于控制游戏难度的中间量。

3. 声明了一些全局变量，包括游戏画面的二维数组 `frame`、小球位置、用户输入字符等。

4. `starup` 函数用于初始化游戏数据，包括设置初始半径、初始分数、木板的位置、时间间隔等。

5. `show` 函数用于在控制台上显示游戏画面，包括小球、木板、边界和分数。

6. `updateWithoutinput` 函数用于更新游戏状态，包括木板上升、小球下落、得分增加、游戏结束等逻辑。

7. `updateWithinput` 函数用于处理用户输入，根据用户按下的键来移动小球。

8. `gotoxy` 函数用于移动光标到指定的控制台屏幕坐标位置，以便在指定位置输出字符。

9. `main` 函数是游戏的入口函数，其中执行了游戏的主循环，不断调用 `show`、`updateWithoutinput` 和 `updateWithinput` 函数以更新和显示游戏画面。
